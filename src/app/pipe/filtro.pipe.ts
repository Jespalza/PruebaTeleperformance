import { Pipe, PipeTransform } from '@angular/core';
// import { getMatIconFailedToSanitizeLiteralError } from '@angular/material';

@Pipe({
  name: 'filterList'
})
export class FilterListPipe implements PipeTransform {

  transform(values:any[], searchText:any): any {
      if(!values || !searchText){
        return values;
      }
      return values.filter(item => {
        return Object.keys(item).some(key => {
          return String(item[key]).toLowerCase().includes(searchText.toLowerCase());
        });
      });
  }

}
