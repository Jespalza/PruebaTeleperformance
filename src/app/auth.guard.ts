import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from "@angular/router";
import { ApiRestService } from './services/apirest.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private ApiRestService : ApiRestService,
    private router : Router
  ){}

  canActivate():boolean{
    if(this.ApiRestService.loggedIn()){
      return true;
    }
    this.router.navigate(['Login']);
    return false;
  }
  
}
