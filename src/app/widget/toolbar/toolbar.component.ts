import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiRestService } from 'src/app/services/apirest.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  titulo;
  constructor(
    private router:Router,
    private api:ApiRestService
    ) { }

  ngOnInit(): void {
    console.log(this.router.url);
    if(this.router.url == "/admin"){
      this.titulo = 'Portal Administrador'
    }else if(this.router.url == '/user'){
      this.titulo = 'Portal Usuario'
    }else{
      this.titulo = ''
    }
  }

  cerrar(){
    this.api.cerrar();
  }


}
