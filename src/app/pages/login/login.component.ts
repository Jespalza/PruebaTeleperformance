import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiRestService } from 'src/app/services/apirest.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user;
  pass
  error
  constructor(
    private router:Router,
    private api:ApiRestService
  ) { }

  ngOnInit(): void {
    this.api.acceso()
  }

  login(){
    if(this.user == 'admin' && this.pass == 'admin'){
      localStorage.setItem('usuario','admin')
      this.router.navigate(['/admin'])
    }else if(this.user=='user'&& this.pass=='user'){
      localStorage.setItem('usuario','user')
      this.router.navigate(['/user'])
    }else{
      console.log("usuario o contraseña incorrecta");
    }
  }
}
