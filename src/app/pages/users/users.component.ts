import { Component, OnInit } from '@angular/core';
import { ApiRestService } from 'src/app/services/apirest.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  ListaComentarios:any=[];
  busqueda
  constructor(
    private api:ApiRestService
  ) { }

  ngOnInit(): void {
    this.api.acceso();
    this.inicio()
  }

  inicio(){
    this.api.Get('posts').subscribe(data=>{
      console.log(data);
      this.ListaComentarios = data;
    })
  }

}
