import { Component, OnInit } from '@angular/core';
import { ApiRestService } from 'src/app/services/apirest.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  ListaUsuarios:any = [];
  constructor(
    private api:ApiRestService
  ) { }

  ngOnInit(): void {
  this.api.acceso()
  this.usuarios()
  }

  usuarios(){
    this.api.Get('users').subscribe(data=>{
      console.log(data);
      this.ListaUsuarios = data
    })
  }
}
