import { Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiRestService {
  urlapi = "https://jsonplaceholder.typicode.com/"
  constructor(
    private http:HttpClient,
    private router:Router
    ){}

  Post(action, json){
    var url = this.urlapi+action
    return this.http.post(url, json)
  }
  Get(action){
    var url = this.urlapi+action
    return this.http.get(url)
  }

  loggedIn(){
    return !!localStorage.getItem('usuario');
  }

  getToken(){
    if(localStorage.getItem('token')){
      return localStorage.getItem('token');
    }else{
      return "null"
    }
  }

  acceso(){
    if(localStorage.getItem('usuario')=='admin'){
      this.router.navigate(['/admin'])
    }else if(localStorage.getItem('usuario')=='user'){
      this.router.navigate(['/user'])
    }
  }

  cerrar(){
    localStorage.clear()
    this.router.navigate(['/Login'])
  }
}


