import { Injectable } from '@angular/core';
import { Request, NextFunction } from "express";
import { HttpInterceptor } from "@angular/common/http";

import { from } from 'rxjs';
import { ApiRestService } from './apirest.service';
@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(
    private api: ApiRestService
  ) { }

  intercept(req:Request,nex:NextFunction){
    const inter = req.clone({
      setHeaders:{
        'auth-token':this.api.getToken()
      }
    })
    return nex.handle(inter)
  }
}
